with open('../day3_input.txt', 'r') as infile:
    input_data = [line.replace('\n', '') for line in infile.readlines()]

oxygen_input = input_data
co2_input = input_data
oxygen = []
co2 = []

for digit in range(len(input_data[0])):
    ones = 0
    zeros = 0  

    print(f'Digit: {digit}')

    for line in oxygen_input:
        if line[digit] == '1':
            ones += 1
        else:
            zeros += 1
    
    if ones >= zeros:
        o2_number = '1'
    else:
        o2_number = '0'
    
    print(f'Ogygen number:{o2_number}')

    if len(oxygen_input) == 1:
        
        # print(oxygen_number)
        break
    else:
        # filter out the data
        oxygen_input = [line for line in oxygen_input if line[digit] == o2_number]

    print(f'Oxygen input: {len(oxygen_input)}')
    # print(f'CO2 input: {co2_input}')

for digit in range(len(input_data[0])):
    ones = 0
    zeros = 0  

    print(f'Digit: {digit}')

    for line in co2_input:
        if line[digit] == '1':
            ones += 1
        else:
            zeros += 1
    
    if ones >= zeros:
        co2_number = '0'
    else:
        co2_number = '1'
    
    print(f'Co2 number:{co2_number}')
    
    if len(co2_input) == 1:
        # co2_number = int(''.join(co2_input[0]), 2)
        print('not filtering')
        break
    else:
        co2_input = [line for line in co2_input if line[digit] == co2_number]
        # for line in co2_input:
        #     if line[digit] != co2_number:
        #         co2_input.remove(line)

    print(f'CO2 input: {co2_input}')

# # gamma_number = int(''.join(oxygen), 2)
# # epsilon_number = int(''.join(co2), 2)

co2_number = int(''.join(co2_input[0]), 2)
oxygen_number = int(''.join(oxygen_input[0]), 2)

print(oxygen_number * co2_number)