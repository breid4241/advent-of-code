with open('../day3_input.txt', 'r') as infile:
    input_data = [line.replace('\n', '') for line in infile.readlines()]


gamma = []
epsilon = []

for digit in range(len(input_data[0])):
    ones = 0
    zeros = 0

    for line in input_data:
        if line[digit] == '1':
            ones += 1
        else:
            zeros += 1
    
    if ones > zeros:
        gamma.append('1')
        epsilon.append('0')
    else:
        gamma.append('0')
        epsilon.append('1')

gamma_number = int(''.join(gamma), 2)
epsilon_number = int(''.join(epsilon), 2)

print(gamma_number * epsilon_number)