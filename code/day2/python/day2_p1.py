with open('../day2_input.txt', 'r') as infile:
    input_data = [line.replace('\n', '') for line in infile.readlines()]

horizontal = 0
depth = 0

for line in input_data:
    line_input = line.split(' ')

    direction = line_input[0]
    amount = int(line_input[1])

    if direction == 'forward':
        horizontal += amount
    elif direction == 'down':
        depth += amount
    elif direction == 'up':
        depth -= amount

result = depth * horizontal

print(result)