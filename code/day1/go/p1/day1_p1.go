package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func main() {
	file, err := os.Open("day1.txt")
	check(err)
	defer file.Close()

	reader := bufio.NewReader(file)

	var previous int64 = 0
	var current_larger int64 = 0

	for {
		line, _, err := reader.ReadLine()
		if err == io.EOF {
			break
		}

		current_depth, _ := strconv.Atoi(string(line))

		if previous == 0 {
			previous = int64(current_depth)
			continue
		}

		if current_depth > int(previous) {
			current_larger = current_larger + 1
		}

		previous = int64(current_depth)

	}

	fmt.Println(current_larger)
}
