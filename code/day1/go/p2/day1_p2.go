package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func main() {
	file, err := os.Open("../../day1.txt")
	check(err)
	defer file.Close()

	scanner := bufio.NewScanner(file)
	scanner.Split(bufio.ScanLines)
	var txtlines []string
	var previous int = 0
	var current int = 0
	var larger int

	for scanner.Scan() {
		txtlines = append(txtlines, scanner.Text())
	}

	for i := 0; i < len(txtlines); i++ {
		if i+1 > len(txtlines) || i+2 == len(txtlines) {
			break
		}

		current, err = strconv.Atoi(txtlines[i])

		if err != nil {
			break
		}
		next, err := strconv.Atoi(txtlines[i+1])

		if err != nil {
			break
		}

		next_next, err := strconv.Atoi(txtlines[i+2])

		if err != nil {
			break
		}

		sum := current + next + next_next

		if previous == 0 {
			previous = sum
			continue
		}

		if sum > previous {
			larger++
		}
		previous = sum
	}

	fmt.Println(larger)
}
