
input_data = []

with open('day1.txt', 'r') as infile:
    input_data = [int(line.replace('\n', '')) for line in infile.readlines()]

# Clean up the data

# print(f'Number of data points: {len(input_data)}')

previous_measurement = 0
current_larger = 0

for current_depth in input_data:
    if previous_measurement == 0:
        previous_measurement = current_depth
        continue
    # print(f'Current depth: {current_depth}')
    if current_depth > previous_measurement:
        # print('Increase in depth')
        current_larger += 1
    
    previous_measurement = current_depth

print(current_larger)