with open('../../day1.txt', 'r') as infile:
    input_data = [int(line.replace('\n', '')) for line in infile.readlines()]

# Clean up the data

print(f'Number of data points: {len(input_data)}')


start = 0

prev_sum = 0
increased = 0

for count in range(len(input_data)):

    try:
        sum = (input_data[start] + input_data[start+1] + input_data[start+2])
    except IndexError:
        continue

    if prev_sum == 0:
        prev_sum = sum
        continue

    if sum > prev_sum:
        increased += 1
    
    prev_sum = sum
    start += 1

print(increased)

